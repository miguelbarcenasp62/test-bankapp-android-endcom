package com.mytest.testtecnocen;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Dashboard extends Fragment {
    private AdapterBalances adapterBalances;
    private AdapterCards adapterCards;
    private AdapterMovimientos adapterMovimientos;
    private RecyclerView rvBalances, rvCards, rvMovimientos;
    private RecyclerView.LayoutManager mLayoutManagerBalances,layoutManagerCards, layoutManagerMovimientos;
    ArrayList<CardsSaldo> listaSaldos;
    ArrayList<Tarjeta> listaTarjetas;
    ArrayList<Transaccion> listaMovimientos;
    TextView nombreCliente, lastSession, addCard;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initVariables(view);
        obtenerDatosApi();
        construirRecyclerViewCards();
        construirRecyclerViewMovimientos();
        return view;
    }
    private void obtenerDatosApi(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://bankapp.endcom.mx/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiRetrofit apiRetrofit = retrofit.create(ApiRetrofit.class);
        Call<Cuenta> getCuenta = apiRetrofit.getCuenta();
        getCuenta.enqueue(new Callback<Cuenta>() {
           @Override
           public void onResponse(Call<Cuenta> call, Response<Cuenta> response) {
               try{
                   if(response.isSuccessful()){
                       Cuenta cuentas = response.body();
                       nombreCliente.setText(cuentas.getCuenta().get(0).getNombre());
                       lastSession.setText(cuentas.getCuenta().get(0).getUltimaSesion());

                   }else{
                       Log.i("API",response.toString());
                   }
               }catch(Exception e){
                   Log.i("API", e.getLocalizedMessage());
               }
           }

           @Override
           public void onFailure(Call<Cuenta> call, Throwable t) {
               Log.i("API",t.getMessage());

           }
       });
        Call<ListaSaldos> getSaldos = apiRetrofit.getSaldos();
        getSaldos.enqueue(new Callback<ListaSaldos>() {
            @Override
            public void onResponse(Call<ListaSaldos> call, Response<ListaSaldos> response) {
                try{
                    if(response.isSuccessful()){
                        ListaSaldos saldos = response.body();
                        listaSaldos.add(new CardsSaldo("Saldo general",saldos.getSaldos().get(0).getSaldoGeneral()));
                        listaSaldos.add(new CardsSaldo("Total de ingresos",saldos.getSaldos().get(0).getIngresos()));
                        listaSaldos.add(new CardsSaldo("Total de gastos",saldos.getSaldos().get(0).getGastos()));
                        construirRecyclerViewSaldos();
                    }else{
                        Log.i("API",response.toString());
                    }
                }catch(Exception e){
                    Log.i("API", e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<ListaSaldos> call, Throwable t) {

            }
        });
        Call<ListaTarjetas> getTarjetas = apiRetrofit.getTarjetas();
        getTarjetas.enqueue(new Callback<ListaTarjetas>() {
            @Override
            public void onResponse(Call<ListaTarjetas> call, Response<ListaTarjetas> response) {
                try{
                    if(response.isSuccessful()){
                        ListaTarjetas tarjetas = response.body();
                        for(int i=0; i< tarjetas.getTarjetas().size();i++){
                            listaTarjetas.add(new Tarjeta(tarjetas.getTarjetas().get(i).getTarjeta(),tarjetas.getTarjetas().get(i).getNombre(),
                                    tarjetas.getTarjetas().get(i).getEstado(),tarjetas.getTarjetas().get(i).getTipo(),tarjetas.getTarjetas().get(i).getId(),
                                    tarjetas.getTarjetas().get(i).getSaldo()));
                        }

                        construirRecyclerViewCards();
                    }else{
                        Log.i("API",response.toString());
                    }
                }catch(Exception e){
                    Log.i("API", e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<ListaTarjetas> call, Throwable t) {

            }
        });
        Call<ListaMovimientos> getMovimientos = apiRetrofit.getMovimientos();
        getMovimientos.enqueue(new Callback<ListaMovimientos>() {
            @Override
            public void onResponse(Call<ListaMovimientos> call, Response<ListaMovimientos> response) {
                try{
                    if(response.isSuccessful()){
                        ListaMovimientos movimientos = response.body();
                        for(int i=0; i< movimientos.getMovimientos().size();i++){
                            listaMovimientos.add(new Transaccion(movimientos.getMovimientos().get(i).getFecha(),
                                    movimientos.getMovimientos().get(i).getDescripcion(), movimientos.getMovimientos().get(i).getTipo(),
                                    movimientos.getMovimientos().get(i).getMonto(), movimientos.getMovimientos().get(i).getId()));
                        }

                        construirRecyclerViewMovimientos();
                    }else{
                        Log.i("API",response.toString());
                    }
                }catch(Exception e){
                    Log.i("API", e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<ListaMovimientos> call, Throwable t) {

            }
        });


    }
    private void initVariables(View view){
        rvBalances =  view.findViewById(R.id.rvBalances);
        rvCards =  view.findViewById(R.id.rvCards);
        rvMovimientos =  view.findViewById(R.id.rvTransactions);
        nombreCliente = view.findViewById(R.id.name);
        lastSession = view.findViewById(R.id.lastSession);
        addCard = view.findViewById(R.id.addCard);
        listaTarjetas = new ArrayList<>();
        listaSaldos = new ArrayList<>();
        listaMovimientos = new ArrayList<>();
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new AddCard(), "addCard");
            }
        });
    }
    private void construirRecyclerViewSaldos(){
        rvBalances.setHasFixedSize(true);
        mLayoutManagerBalances = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL,false);
        adapterBalances = new AdapterBalances(this,listaSaldos);
        rvBalances.setLayoutManager(mLayoutManagerBalances);
        rvBalances.setAdapter(adapterBalances);
    }
    private void construirRecyclerViewCards(){
        rvCards.setHasFixedSize(true);
        layoutManagerCards = new LinearLayoutManager(getActivity());
        adapterCards = new AdapterCards(this,listaTarjetas);
        rvCards.setLayoutManager(layoutManagerCards);
        rvCards.setAdapter(adapterCards);
    }
    private void construirRecyclerViewMovimientos () {
        rvMovimientos.setHasFixedSize(true);
        layoutManagerMovimientos = new LinearLayoutManager(getActivity());
        adapterMovimientos = new AdapterMovimientos(this,listaMovimientos);
        rvMovimientos.setLayoutManager(layoutManagerMovimientos);
        rvMovimientos.setAdapter(adapterMovimientos);
    }
    private void replaceFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag);
        fragmentTransaction.commit();
    }
}
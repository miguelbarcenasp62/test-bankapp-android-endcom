package com.mytest.testtecnocen;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiRetrofit {
        @GET("/api/bankappTest/cuenta")
        Call<Cuenta> getCuenta();

        @GET("/api/bankappTest/saldos")
        Call<ListaSaldos> getSaldos();

        @GET("/api/bankappTest/tarjetas")
        Call<ListaTarjetas> getTarjetas();

        @GET("/api/bankappTest/movimientos")
        Call<ListaMovimientos> getMovimientos();
}

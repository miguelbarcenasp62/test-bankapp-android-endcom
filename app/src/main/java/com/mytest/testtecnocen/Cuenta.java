package com.mytest.testtecnocen;

import java.util.List;

public class Cuenta {
    private List<Persona> cuenta;

    public Cuenta() {
    }

    public Cuenta(List<Persona> cuenta) {
        this.cuenta = cuenta;
    }

    public List<Persona> getCuenta() {
        return cuenta;
    }

    public void setCuenta(List<Persona> cuenta) {
        this.cuenta = cuenta;
    }
}

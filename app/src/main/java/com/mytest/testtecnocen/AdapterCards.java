package com.mytest.testtecnocen;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterCards extends RecyclerView.Adapter<AdapterCards.ViewHolder>{
    ArrayList<Tarjeta> listaTarjetas;
    Dashboard context;
    public AdapterCards(Dashboard context,  ArrayList<Tarjeta> listaTarjetas) {
        this.listaTarjetas = listaTarjetas;
        this.context=context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView cardStatus, cardNumber,cardOwner,cardType,cardBalance;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            cardStatus = itemView.findViewById(R.id.cardStatus);
            cardNumber = itemView.findViewById(R.id.cardNumber);
            cardOwner = itemView.findViewById(R.id.cardOwner);
            cardType = itemView.findViewById(R.id.cardType);
            cardBalance = itemView.findViewById(R.id.cardBalance);
        }
    }
    @NonNull
    @Override
    public AdapterCards.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(context.getActivity()).inflate(R.layout.row_cards, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new AdapterCards.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterCards.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        viewHolder.cardStatus.setText(listaTarjetas.get(i).getEstado());
        viewHolder.cardNumber.setText(listaTarjetas.get(i).getTarjeta());
        viewHolder.cardOwner.setText(listaTarjetas.get(i).getNombre());
        viewHolder.cardType.setText(listaTarjetas.get(i).getTipo());
        viewHolder.cardBalance.setText(formatearBalance(String.valueOf(listaTarjetas.get(i).getSaldo())));

    }

    private String formatearBalance(String saldo){

        String pattern = "$###,###,###.##";
        double value = Double.parseDouble(saldo);
        //Si no le paso ningun Locale, toma el del sistema, que en mi caso es Locale("es","MX");
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }
    @Override
    public int getItemCount() {
        return listaTarjetas.size();
    }
}


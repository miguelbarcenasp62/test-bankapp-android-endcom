package com.mytest.testtecnocen;

public class CardsSaldo {
    private String titulo;
    private float balance;

    public CardsSaldo() {
    }

    public CardsSaldo(String titulo, float balance) {
        this.titulo = titulo;
        this.balance = balance;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}

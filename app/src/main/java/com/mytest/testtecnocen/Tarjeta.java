package com.mytest.testtecnocen;

public class Tarjeta {
    private String tarjeta, nombre, estado, tipo;
    private long id;
    private float saldo;

    public Tarjeta() {
    }

    public Tarjeta(String tarjeta, String nombre, String estado, String tipo, long id, float saldo) {
        this.tarjeta = tarjeta;
        this.nombre = nombre;
        this.estado = estado;
        this.tipo = tipo;
        this.id = id;
        this.saldo = saldo;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}

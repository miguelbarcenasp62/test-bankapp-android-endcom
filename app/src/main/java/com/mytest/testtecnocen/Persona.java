package com.mytest.testtecnocen;

public class Persona {
    private String cuenta, nombre, ultimaSesion;
    private long id;

    public Persona() {
    }

    public Persona(String cuenta, String nombre, String ultimaSesion, long id) {
        this.cuenta = cuenta;
        this.nombre = nombre;
        this.ultimaSesion = ultimaSesion;
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

package com.mytest.testtecnocen;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterMovimientos extends RecyclerView.Adapter<AdapterMovimientos.ViewHolder>{
    ArrayList<Transaccion> listaMovimientos;
    Dashboard context;
    public AdapterMovimientos(Dashboard context,  ArrayList<Transaccion> listaMovimientos) {
        this.listaMovimientos = listaMovimientos;
        this.context=context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView descripcion, fecha,cantidad;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.description);
            fecha = itemView.findViewById(R.id.date);
            cantidad = itemView.findViewById(R.id.cantidadTransaccion);
        }
    }
    @NonNull
    @Override
    public AdapterMovimientos.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(context.getActivity()).inflate(R.layout.row_transaction, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new AdapterMovimientos.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterMovimientos.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        viewHolder.descripcion.setText(listaMovimientos.get(i).getDescripcion());
        viewHolder.fecha.setText(listaMovimientos.get(i).getFecha());
        if(listaMovimientos.get(i).getTipo().equalsIgnoreCase("abono")){
            viewHolder.cantidad.setText(formatearBalance(String.valueOf(listaMovimientos.get(i).getMonto())));
            viewHolder.cantidad.setTextColor(Color.parseColor("#00b894"));

        }else if(listaMovimientos.get(i).getTipo().equalsIgnoreCase("cargo")){
            viewHolder.cantidad.setText(formatearBalance(String.valueOf(listaMovimientos.get(i).getMonto())));
            viewHolder.cantidad.setTextColor(Color.parseColor("#d63031"));
        }

    }

    private String formatearBalance(String saldo){

        String pattern = "$###,###,###.##";
        double value = Double.parseDouble(saldo);
        //Si no le paso ningun Locale, toma el del sistema, que en mi caso es Locale("es","MX");
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }
    @Override
    public int getItemCount() {
        return listaMovimientos.size();
    }
}

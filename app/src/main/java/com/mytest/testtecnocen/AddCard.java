package com.mytest.testtecnocen;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;


public class AddCard extends Fragment {
    View view;
    TextView tarjeta, cuenta, nombre, issure, marca, estatus, saldo, tipo_cuenta;
    Button cancelar, agregar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_card, container, false);
        initViews();
        actions();
        return view;
    }
    private void initViews(){
        tarjeta = view.findViewById(R.id.tarjeta_edittxt);
        cuenta = view.findViewById(R.id.cuenta_edittxt);
        nombre = view.findViewById(R.id.nombre_edittxt);
        issure = view.findViewById(R.id.issure_edittxt);
        marca = view.findViewById(R.id.marca_edittxt);
        estatus = view.findViewById(R.id.estatus_edittxt);
        saldo = view.findViewById(R.id.saldo_edittxt);
        tipo_cuenta = view.findViewById(R.id.tipo_edittxt);
        cancelar = view.findViewById(R.id.cancelar);
        agregar = view.findViewById(R.id.agregar);

    }
    private void actions(){
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new Dashboard(),"dashboard");
            }
        });

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String json = gson.toJson(new NuevaTarjeta(tarjeta.getText().toString(),cuenta.getText().toString(),issure.getText().toString(),nombre.getText().toString(), marca.getText().toString(), estatus.getText().toString(), tipo_cuenta.getText().toString(), Float.parseFloat(saldo.getText().toString())));
                Toast.makeText(getActivity(), json, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void replaceFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag);
        fragmentTransaction.commit();
    }

}
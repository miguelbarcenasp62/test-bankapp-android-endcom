package com.mytest.testtecnocen;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterBalances extends RecyclerView.Adapter<AdapterBalances.ViewHolder>{
    ArrayList<CardsSaldo> listaCardsSaldos;
    Dashboard context;
    public AdapterBalances(Dashboard context,  ArrayList<CardsSaldo> listaCardsSaldos) {
        this.listaCardsSaldos = listaCardsSaldos;
        this.context=context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, balance;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.accountTitle);
            balance = itemView.findViewById(R.id.balanceInAccount);
        }




    }
    @NonNull
    @Override
    public AdapterBalances.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(context.getActivity()).inflate(R.layout.row_balances, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterBalances.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        viewHolder.title.setText(listaCardsSaldos.get(i).getTitulo());
        viewHolder.balance.setText(formatearBalance(String.valueOf(listaCardsSaldos.get(i).getBalance())));

    }

    private String formatearBalance(String CardsSaldo){

        String pattern = "$###,###,###.##";
        double value = Double.parseDouble(CardsSaldo);
        //Si no le paso ningun Locale, toma el del sistema, que en mi caso es Locale("es","MX");
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }
    @Override
    public int getItemCount() {
        return listaCardsSaldos.size();
    }
}

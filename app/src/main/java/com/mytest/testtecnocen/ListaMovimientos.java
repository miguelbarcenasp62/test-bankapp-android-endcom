package com.mytest.testtecnocen;

import java.util.List;

public class ListaMovimientos {
    private List<Transaccion> movimientos;

    public ListaMovimientos() {
    }

    public ListaMovimientos(List<Transaccion> movimientos) {
        this.movimientos = movimientos;
    }

    public List<Transaccion> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<Transaccion> movimientos) {
        this.movimientos = movimientos;
    }
}

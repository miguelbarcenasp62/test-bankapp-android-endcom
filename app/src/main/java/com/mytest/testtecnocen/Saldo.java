package com.mytest.testtecnocen;

public class Saldo {
    private String cuenta;
    private float saldoGeneral,ingresos, gastos;
    private long id;

    public Saldo() {
    }

    public Saldo(String cuenta, float saldoGeneral, float ingresos, float gastos, long id) {
        this.cuenta = cuenta;
        this.saldoGeneral = saldoGeneral;
        this.ingresos = ingresos;
        this.gastos = gastos;
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public float getSaldoGeneral() {
        return saldoGeneral;
    }

    public void setSaldoGeneral(float saldoGeneral) {
        this.saldoGeneral = saldoGeneral;
    }

    public float getIngresos() {
        return ingresos;
    }

    public void setIngresos(float ingresos) {
        this.ingresos = ingresos;
    }

    public float getGastos() {
        return gastos;
    }

    public void setGastos(float gastos) {
        this.gastos = gastos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

package com.mytest.testtecnocen;

import java.util.List;

public class ListaTarjetas {
    private List<Tarjeta> tarjetas;

    public ListaTarjetas() {
    }

    public ListaTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public List<Tarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }
}

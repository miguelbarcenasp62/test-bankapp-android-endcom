package com.mytest.testtecnocen;

import java.util.List;

public class ListaSaldos {
    private List<Saldo> saldos;

    public ListaSaldos() {
    }

    public ListaSaldos(List<Saldo> saldos) {
        this.saldos = saldos;
    }

    public List<Saldo> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<Saldo> saldos) {
        this.saldos = saldos;
    }
}

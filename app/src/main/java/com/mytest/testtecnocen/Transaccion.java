package com.mytest.testtecnocen;

public class Transaccion {
    private String fecha, descripcion, tipo;
    private float monto;
    private long id;

    public Transaccion() {
    }

    public Transaccion(String fecha, String descripcion, String tipo, float monto, long id) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.monto = monto;
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

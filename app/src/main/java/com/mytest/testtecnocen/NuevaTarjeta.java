package com.mytest.testtecnocen;

public class NuevaTarjeta {
    private String tarjeta, cuenta, issure, nombre, marca, estatus, tipo_cuenta;
    private float saldo;

    public NuevaTarjeta() {
    }

    public NuevaTarjeta(String tarjeta, String cuenta, String issure, String nombre, String marca, String estatus, String tipo_cuenta, float saldo) {
        this.tarjeta = tarjeta;
        this.cuenta = cuenta;
        this.issure = issure;
        this.nombre = nombre;
        this.marca = marca;
        this.estatus = estatus;
        this.tipo_cuenta = tipo_cuenta;
        this.saldo = saldo;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getIssure() {
        return issure;
    }

    public void setIssure(String issure) {
        this.issure = issure;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}
